$('.slide').carousel({
    interval: 3000
  });

  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });

  $('.popover-dismiss').popover({
    trigger: 'focus'
  });

  $('#myModal').on('shown.bs.modal', function () {
    $('#myInput').trigger('focus')
  });